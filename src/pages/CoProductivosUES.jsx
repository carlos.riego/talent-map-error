import React from 'react'
import { useState, useEffect } from 'react'
//import useColaboradores from '../hooks/useColaboradores'
import useCoProductivosUES from '../hooks/useCoProductivosUES'
import useAuth from '../hooks/useAuth'
import axios from 'axios'
import { useNavigate, useLocation } from 'react-router-dom'
import { Button, Modal, TextInput } from 'flowbite-react'


const CoProductivosUES = () => {
  const navigate = useNavigate()
  const { coProductivosUEN, cargando } = useCoProductivosUES()
  const [email, setEmail] = useState('')
  const auth = JSON.parse(localStorage.getItem("Auth"))
  const [openModal, setOpenModal] = useState(false);
  const [productivo, setProductivo] = useState([])
  const [searchBar, setSearchBar] = useState('')
  const [eliminado, setEliminado] = useState([])

  console.log(coProductivosUEN)


  const verMapa = async (mail) => {

    localStorage.setItem('mail', mail)



    navigate('/colaboradores/mapa')

  }

  useEffect(() => {

  }, [openModal])

  const onSearch = e => {
    setSearchBar(e.target.value)
  }

  const handleDelete = async (id) => {

    try {
      const token = localStorage.getItem("token")

      const cofing = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }
      const { data } = await axios.post(`https://apimapatalentos.onrender.com/api/coProductivosUES/eliminarUES/${id}`, cofing)
      setEliminado(data)
      window.location.reload()
    } catch (error) {
      console.log(error)
    }
  }

  const filteredColaboradores = Object.values(coProductivosUEN).filter((colaborador) =>
    colaborador.staff.toLowerCase().includes(searchBar.toLowerCase())

  )



  if (cargando) return
  <>
    <button disabled type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 inline-flex items-center">
      <svg aria-hidden="true" role="status" class="inline w-4 h-4 me-3 text-white animate-spin" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="#E5E7EB" />
        <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentColor" />
      </svg>
      Cargando...
    </button>

  </>



  return (

    <>
      <div className="carousel flex justify-center rounded-md overflow-hidden">
        <div className="carousel-item -mb-24 h-1/2">
          <img src="https://i.imgur.com/RqAa1Fy.png" alt="Burger" className='h-2/4' />
        </div>
      </div>
      <div className='flex justify-end mb-7 mr-10'>
        <div className="max-w-md">
          <div className='flex'>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-10 h-10">
              <path strokeLinecap="round" strokeLinejoin="round" d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z" />
            </svg>

            <TextInput size={'lg'} id="email4" type="email" placeolder="Filtrar por nombre" onChange={onSearch} required />
          </div>
        </div>
      </div>
      <div className="grid grid-cols-4 gap-4 mx-10">
      {filteredColaboradores.map((elemento, index) => (
        <>
          <div class="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700" >
            <div class="flex justify-end px-4 pt-4">
            </div>
            <div class="flex flex-col items-center pb-10">
              <img class="w-25 h-20 mb-3 rounded-full shadow-lg" src={elemento.profile} alt="Imagen de perfil" />
              <h5 class="mb-1 text-xl font-medium text-gray-900 dark:text-white">{elemento.staff}</h5>
              <span class="text-sm text-gray-500 dark:text-gray-400">{elemento.rol}</span>
              <div class="flex mt-4 md:mt-6">
                <Button className=' bg-ayi hover:bg-ayi text-white' onClick={() => { setOpenModal(true), setProductivo(elemento) }}>Ver mas</Button>
                {auth.isAdmin ?
                <>
                <button onClick={() => verMapa(elemento.mail)} class="py-2 px-4 ms-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Ver Mapa</button>
                <Button className=" bg-red-600 hover:bg-red-700 text-white ml-3" onClick={() => { handleDelete(elemento._id) }}>Eliminar</Button>
                </>
                 : null}
              </div>
              <Modal show={openModal} onClose={() => setOpenModal(false)}>
                <Modal.Header><h3 className="font-bold text-lg">{`Datos de ${productivo.staff}`}</h3></Modal.Header>
                <Modal.Body>
                  <div className="space-y-6">
                    <ul class="max-w-md space-y-1 list-none list-inside dark:text-gray-400">
                      <li class=" font-bold text-lg">
                        {`Legajo: ${productivo.legajo}`}
                      </li>
                      <li class=" font-bold text-lg">
                        {`Rol: ${productivo.rol}`}
                      </li>
                      <li class=" font-bold text-lg">
                        {`Manager: ${productivo.manager}`}
                      </li>
                      <li class=" font-bold text-lg">
                        {`Lider: ${productivo.lider}`}
                      </li>
                      <li class=" font-bold text-lg">
                        {`Lugar: ${productivo.lugar}`}
                      </li>
                      <li class=" font-bold text-lg">
                        {`Ingreso: ${productivo.fechaDeIngreso}`}
                      </li>
                      <li class=" font-bold text-lg">
                        {`UES: ${productivo.ues}`}
                      </li>
                      <li class=" font-bold text-lg">
                        {`Mail: ${productivo.mail}`}
                      </li>
                    </ul>
                  </div>
                </Modal.Body>
                <Modal.Footer>
                  <Button color="gray" onClick={() => setOpenModal(false)}>
                    Cerrar
                  </Button>
                </Modal.Footer>
              </Modal>
            </div>
          </div>
        </>
      ))}
      </div>

    </>
  )
}

export default CoProductivosUES
