import React, { useState } from "react"
import { Navigate, useNavigate } from "react-router-dom"
import axios from "axios"
import useAuth from "../hooks/useAuth"
import Alerta from "../components/Alerta"

const Login = () => {

    const navigate = useNavigate()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [alerta, setAlerta] = useState({})
    const { setAuth } = useAuth()


    const handleSubmit = async e => {
        e.preventDefault()
        if ([email, password].includes('')) {
            setAlerta({
                msg: 'Todos los campos son obligatorios',
                error: true
            })
            return
        }
        try {

            const cofing = {
                headers: {
                    "Content-Type": "application/json",
                    'Access-Control-Allow-Origin': '*'
                }
            }

            const { data } = await axios.post("https://apimapatalentos.onrender.com/api/usuarios/login/", { email, password }, cofing)
            localStorage.setItem('token', data.token)
            setAuth(data)
            localStorage.setItem('Auth', JSON.stringify(data))
            navigate("/colaboradores")
        } catch (error) {
            setAlerta({
                msg: 'Usuario o Contraseña incorrectos',
                error: true
            })
        }

    }

    const { msg } = alerta
    return (
        <>
            <div className="bg-ayi flex w-full h-1/6 rounded-md mb-4">
                <img
                    src="https://ayi.group/wp-content/uploads/2020/12/ayi-logo-blanco.png"
                    className="mr-3 h-6 sm:h-11 m-2"
                />
                <p className=" m-2 text-4xl font-semibold text-white">
                    Mapa de Talentos
                </p>
            </div>
            {msg && <Alerta alerta={alerta} />}
            <form onSubmit={handleSubmit} action="" className=" my-1 bg-gray-200 shadow rounded-lg px-10 py-5">
                <div className=" my-5">
                    <label htmlFor="email" className=" uppercase text-gray-700 block text-xl font-bold">Email</label>
                    <input value={email} onChange={e => setEmail(e.target.value)} id="email" type="email" placeholder="Email" className=" w-full mt-3 p-3 border rounded-xl bg-gray-50" />
                </div>
                <div className=" my-5">
                    <label htmlFor="password" className=" uppercase text-gray-700 block text-xl font-bold">Contraseña</label>
                    <input value={password} onChange={e => setPassword(e.target.value)} id="password" type="password" placeholder="Contraseña" className=" w-full mt-3 p-3 border rounded-xl bg-gray-50" />
                </div>

                <input type="submit" value="Iniciar Sesion" className=" bg-ayi w-full py-3 text-white uppercase font-bold rounded hover:cursor-pointer
             hover:bg-sky-800 transition-colors"></input>
            </form>
        </>
    )
}

export default Login
