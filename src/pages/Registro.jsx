import React, { useState, useEffect } from "react";
import { Button, Checkbox, Label, TextInput } from "flowbite-react"
import { useLocation } from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css"
import { registerLocale, setDefaultLocale } from "react-datepicker";
import { es } from 'date-fns/locale/es';
import axios from "axios"
//import ToastComponent from "../components/ToastComponent";

registerLocale('es', es)


const Registro = () => {

    const [contraseña, setContraseña] = useState("")
    const [nombre, setNombre] = useState("")
    const [email, setEmail] = useState("")
    const [rol, setRol] = useState("")
    const [admin, setAdmin] = useState(false)
    const [legajo, setLegajo] = useState("")
    const [puesto, setPuesto] = useState("")
    const [lider, setLider] = useState("")
    const [lugar, setLugar] = useState("")
    const [proyecto, setProyecto] = useState("")
    const [fecha, setFecha] = useState(new Date())
    const [categoria, setCategoria] = useState("")
    const [seniority, setSeniority] = useState("")
    const [tecnologia, setTecnologia] = useState("")
    const [manager, setManager] = useState("")
    const [cliente, setCliente] = useState("")
    const [error, setError] = useState(false)
    const [exito, setExito] = useState(false)
    const [registro, setRegistro] = useState(false)

    const handleUpdate = async e => {
        e.preventDefault()
        const token = localStorage.getItem('token')
        const config = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
        }
        const register = {
            email: email,
            password: contraseña,
            isAdmin: admin

        }
        const data = {
            colaborador: nombre,
            mail: email,
            legajo: legajo,
            puesto: puesto,
            rol: rol,
            lugar: lugar,
            lider: lider,
            proyecto: proyecto,
            fechaDeIngreso: fecha.toDateString(),
            seniority: seniority,
            tecnologia: tecnologia,
            manager: manager,
            clientePP: cliente,
            perfil: "https://i.imgur.com/dsW5FAb.jpeg"
        }
        const dataUES = {
            staff: nombre,
            mail: email,
            legajo: legajo,
            rol: rol,
            lugar: lugar,
            lider: lider,
            fechaDeIngreso: fecha.toDateString(),
            manager: manager,
            profile: "https://i.imgur.com/dsW5FAb.jpeg"
        }

        try {

            axios.post(`https://apimapatalentos.onrender.com/api/usuarios/registro`, register, config)

            setRegistro(true)
        }
        catch (error) {

            console.log(error)

            setError(true)
        }


        if (registro && categoria === "UEN") {
            console.log(data)

            try {

                await axios.post(`https://apimapatalentos.onrender.com/api/productivosUEN/darAltaUEN`, data, config)
                console.log("Es UEN")
                setExito(true)
                history.pushState({}, "", "http://localhost:5173/colaboradores")
            }
            catch (error) {
                console.log(error)
                setError(true)
            }
        }

        if (registro && categoria === "UES") {
            try {
                axios.post(`https://apimapatalentos.onrender.com/api/coProductivosUES/darAltaUES`, dataUES, config)
                console.log("Es UES")
                setExito(true)
            }
            catch (error) {
                console.log(error)
                setError(false)
            }
        }




    }

    useEffect(() => {
        console.log("Error")
        setExito(false)
    }, [error])

    useEffect(() => {
        console.log("Exito")
        setError(false)
    }, [exito])



    return (
        <div className="flex justify-center items-center h-screen ">
            <div>

                <h1 className=" text-4xl font-bold  mb-20">Registrar nuevo usuario</h1>

                {categoria === "" ?
                    <div>
                        <div className="mb-2 block text-center">
                            <Label htmlFor="cate" value="Categoria" className=" text-lg" />
                        </div>
                        <select className="select select-bordered w-full max-w-xs" id="cate" onChange={e => setCategoria(e.target.value)}>
                            <option disabled selected value="" > -- Seleccionar una opción -- </option>
                            <option>UEN</option>
                            <option>UES</option>
                        </select>
                    </div>
                    : null}
                {categoria != "" ?
                    <form className="max-w-md grid grid-cols-2 gap-4 " onSubmit={handleUpdate}>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="nombre" value="Nombre" />
                            </div>
                            <TextInput id="nombre" type="text" placeholder="Nombre y Apellido" required onChange={e => setNombre(e.target.value)} />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="legajo" value="Legajo" />
                            </div>
                            <TextInput id="legajo" type="text" placeholder="Legajo" required onChange={e => setLegajo(e.target.value)} />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="email1" value="Correo" />
                            </div>
                            <TextInput id="email1" type="email" placeholder="Email" required onChange={e => setEmail(e.target.value)} />
                        </div>
                        {categoria === "UEN" ?
                            <>
                                <div>
                                    <div className="mb-2 block">
                                        <Label htmlFor="puesto" value="Puesto" />
                                    </div>
                                    <TextInput id="puesto" type="text" placeholder="Puesto" required onChange={e => setPuesto(e.target.value)} />
                                </div>
                                <div>
                                    <div className="mb-2 block">
                                        <Label htmlFor="seniority" value="Seniority" />
                                    </div>
                                    <TextInput id="seniority" type="text" placeholder="Seniority" required onChange={e => setSeniority(e.target.value)} />
                                </div>

                                <div>
                                    <div className="mb-2 block">
                                        <Label htmlFor="tecnologia" value="Tecnologia" />
                                    </div>
                                    <TextInput id="tecnologia" type="text" placeholder="Tecnologia" required onChange={e => setTecnologia(e.target.value)} />
                                </div>
                            </>
                            : null}
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="manager" value="Manager" />
                            </div>
                            <TextInput id="manager" type="text" placeholder="Manager" required onChange={e => setManager(e.target.value)} />
                        </div>
                        {categoria === "UEN" ?
                            <div>
                                <div className="mb-2 block">
                                    <Label htmlFor="cliente" value="Cliente PP" />
                                </div>
                                <TextInput id="cliente" type="text" placeholder="Cliente PP" required onChange={e => setCliente(e.target.value)} />
                            </div>
                            : null}
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="rol" value="Rol" />
                            </div>
                            <TextInput id="rol" type="text" placeholder="Rol" required onChange={e => setRol(e.target.value)} />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="lugar" value="Lugar" />
                            </div>
                            <TextInput id="lugar" type="text" placeholder="Lugar" required onChange={e => setLugar(e.target.value)} />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="lider" value="Lider" />
                            </div>
                            <TextInput id="lider" type="text" placeholder="Lider" required onChange={e => setLider(e.target.value)} />
                        </div>
                        {categoria === "UEN" ?
                            <div>
                                <div className="mb-2 block">
                                    <Label htmlFor="proyecto" value="Proyecto" />
                                </div>
                                <TextInput id="proyecto" type="text" placeholder="Proyecto" required onChange={e => setProyecto(e.target.value)} />
                            </div>
                            : null}
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="password1" value="Contraseña" />
                            </div>
                            <TextInput id="password1" type="password" required onChange={e => setContraseña(e.target.value)} />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="password1" value="Fecha de ingreso" />
                            </div>
                            <DatePicker dateFormat="dd/MM/YYYY" locale="es" selected={fecha} onChange={(date) => setFecha(date)} />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="admin" value="Administrador?" />
                            </div>
                            <select className="select select-bordered w-full max-w-xs" id="admin" onChange={e => setAdmin(e.target.value)}>

                                <option selected value={false}>No</option>
                                <option value={true}>Si</option>
                            </select>
                        </div>
                        <Button type="submit">Submit</Button>
                        <Button onClick={() => location.reload()}>Volver</Button>
                    </form>
                    : null}
            </div>
        </div>
    )

}

export default Registro