import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Spinner } from 'flowbite-react';
import Modals from '../components/Modal';
import { Button, Card, Avatar } from "flowbite-react";


const Mapa = () => {
    const [productivo, setProductivo] = useState({});
    const [data, setData] = useState({});
    const [cargando, setCargando] = useState(true);
    const Auth = JSON.parse(localStorage.getItem("Auth"))

    const obtenerProductivo = async () => {
        try {
            const token = localStorage.getItem('token');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            };

            const response = await axios.get('https://apimapatalentos.onrender.com/api/productivosUEN/buscarProductivo', config);
            setProductivo(response.data);
            console.log('Productivo:', response.data);
        } catch (error) {
            console.log('Error al obtener productivo:', error);
        }
    };

    const obtenerTalento = async () => {
        try {
            const token = localStorage.getItem('token');
            const mail = localStorage.getItem('mail');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            };

            const response = await axios.get(`https://apimapatalentos.onrender.com/api/talentos/ver/${mail}`, config);
            setData(response.data);
            console.log('Talentos:', response.data);
        } catch (error) {
            console.log('Error al obtener talentos:', error);
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                await Promise.all([obtenerProductivo(), obtenerTalento()]);
                setCargando(false);
            } catch (error) {
                console.log('Error en useEffect:', error);
                setCargando(false);
            }
        };

        fetchData();
    }, []);


    if (cargando) return <div className="flex justify-center mt-10"><Spinner aria-label="Cargando..." /></div>;

    return (
        <div className="grid grid-cols-1 gap-4 place-items-center mt-10">

            <Modals competencia={"idiomas"} />
            <div className="w-screen">
                <Modals competencia={"frameworks"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"versionadores"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"integracion"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"bd"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"cloud"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"cicd"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"seguridad"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"arquitectura"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"uxui"} />
            </div>
            <div className="w-screen">
                <Modals competencia={"gestion"} />
            </div>
        </div>
    );
};

export default Mapa;
