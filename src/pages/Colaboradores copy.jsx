import React from 'react'
import { useState } from 'react'
import useColaboradores from '../hooks/useColaboradores'
import useAuth from '../hooks/useAuth'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { Card } from 'flowbite-react'


const Colaboradores = () => {
  const navigate = useNavigate()
  const { colaboradores, cargando } = useColaboradores()
  const [email, setEmail] = useState('')
  const { auth } = useAuth()
  console.log(colaboradores)
  console.log(Object.values(colaboradores)[0])
  console.log(Object.values(colaboradores).length)

  const verMapa = async (mail) => {

    localStorage.setItem('mail', mail)



    navigate('mapa')

  }


  if (cargando) return 'Cargando...'



  return (



    <>
      {Object.keys(colaboradores).length === 0 ? window.location.reload() : <div>
      <div class="flex flex-col">
        <div class="overflow-x-auto">
          <div class="inline-block min-w-full align-middle">
            <div class="overflow-hidden shadow">
              <table class="min-w-full divide-y divide-gray-200 table-fixed dark:divide-gray-600">
                <thead class="bg-gray-100 dark:bg-gray-700">
                  <tr>

                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Colaborador
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Legajo
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Rol
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Cliente PP
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Manager
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Lider
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Lugar
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Tecnologia
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Ingreso
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Seniority
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      UEN/UES
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Mail
                    </th>
                    <th scope="col" class="p-4 text-xs font-medium text-left text-gray-500 uppercase dark:text-gray-400">
                      Mapa
                    </th>
                  </tr>
                </thead>
                {Object.values(colaboradores).map((elemento, index) => {
                  return (
                    <tbody class="bg-white divide-y divide-gray-200 dark:bg-gray-800 dark:divide-gray-700">
                      <tr class="hover:bg-gray-100 dark:hover:bg-gray-700">

                        <td class="flex items-center p-2 mr-10 space-x-6 whitespace-nowrap">
                          <div class="text-sm font-normal text-gray-500 dark:text-gray-400">
                            <div class="text-base font-semibold text-gray-900 dark:text-white">{elemento.colaborador}</div>
                          </div>
                        </td>
                        <td class="max-w-sm p-2 overflow-hidden text-base font-normal text-gray-500 truncate xl:max-w-xs dark:text-gray-400">{elemento.legajo}</td>
                        <td class="p-2 text-base font-medium text-gray-900 whitespace-nowrap dark:text-white">{elemento.rol}</td>
                        <td class="p-2 text-base font-medium text-gray-900 whitespace-nowrap dark:text-white">{elemento.cliente_pp}</td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.manager}</p>
                          </div>
                        </td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.lider}</p>
                          </div>
                        </td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.lugar}</p>
                          </div>
                        </td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.tecnologia}</p>
                          </div>
                        </td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.ingreso}</p>
                          </div>
                        </td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.seniority}</p>
                          </div>
                        </td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.UEN_UES}</p>
                          </div>
                        </td>
                        <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <p>{elemento.mail}</p>
                          </div>
                        </td>
                        {auth.isAdmin ? <td class="p-2 text-base font-normal text-gray-900 whitespace-nowrap dark:text-white">
                          <div class="flex items-center">
                            <button onClick={() => verMapa(elemento.mail)}>Ver Mapa</button>
                          </div>
                        </td> : null }
                      </tr>
                    </tbody>
                  )})}
                </table>
            </div>
          </div>
        </div>
      </div>
      </div >
      }
    </>

    
  )
}

export default Colaboradores
