import React from 'react';

import axios from "axios"
import { useEffect, useState } from "react"
import { Button, Modal } from 'flowbite-react'
import useAuth from '../hooks/useAuth'


const BuscarTalento2 = () => {

    const [searchTerm, setSearchTerm] = useState('');
    const { auth } = useAuth()
    const [colaboradorResult, setColaboradorResult] = useState([])
    const [searchResults, setSearchResults] = useState([]);
    const [mailResults, setMailResults] = useState([])
    const [mailSearch, setMailSearch] = useState('')
    const [isLoading, setIsLoading] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [productivo, setProductivo] = useState([])
    const [productivoResults, setProductivoResults] = useState([])


    const busqueda = () => {
        Object.values(mailResults).map(async (user) => {
            //const apiUrl2 = `http://localhost:4000/api/productivosUEN/${user.mail}`;
            const apiUrl2 = `https://apimapatalentos.onrender.com/api/productivosUEN/${user.mail}`;
            try {
                const token = localStorage.getItem("token")
                console.log(apiUrl2)
                const cofing = {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`
                    }
                }
                if (productivoResults) {
                    const { data } = await axios.get(apiUrl2, cofing)
                    setProductivoResults(productivoResults => [...productivoResults, data])
                    console.log(productivoResults)
                    //setProductivoResults(data)
                    setIsLoading(false)
                }
            } catch (error) {
                console.log(error)
            }


        })



    }


    useEffect(() => {
        if (searchTerm === '') {
            setSearchResults([]);
            setProductivoResults([])
            return;
        }

        setIsLoading(false);




        const apiUrl = `https://apimapatalentos.onrender.com/api/talentos/buscar/${searchTerm}`;
        //const apiUrl = `http://localhost:4000/api/talentos/buscar/${searchTerm}`;
        const token = localStorage.getItem("token")

        fetch(apiUrl, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        })
            .then((response) => response.json())

            .then((data) => {
                setSearchResults(data)
                setMailResults(data)
                console.log(data)
                //setSearchResults(data.items); // En este caso, la API de GitHub devuelve una lista de usuarios
                //setIsLoading(false);

            })
            .catch((error) => {
                console.error('Error fetching data:', error);
                setIsLoading(false);
            });


    }, [searchTerm]);

    useEffect(() => {

        if (Object.keys(searchResults).length > 0) {

            busqueda()
            mailResults.forEach(element => {
                setMailSearch(element.mail)
            });

        }
        else {
            setProductivoResults([])
        }
    }, [searchResults])

    useEffect(() => {
        console.log(openModal)
    }, [openModal])



    const handleChange = (event) => {
        event.preventDefault()
        setSearchTerm(event.target.value);
        console.log(event.target.value)
    };


    if (isLoading) {
        return <div>Cargando...</div>
    }

    return (
        <>
            <div className="carousel flex justify-center rounded-md overflow-hidden">
                <div className="carousel-item -mb-20 h-1/2 ">
                    <img src="https://i.imgur.com/SFfRgJb.jpeg" alt="Burger" className='h-2/4 mt-14 mb-10' />
                </div>
            </div>
            <div className=" my-10 mx-10">
                <form>
                    <label htmlFor="default-search" className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                    <div className="relative">
                        <input onChange={handleChange} type="search" id="default-search" className="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Buscar colaboradores por talento... GIT, Javascript, etc" required />
                    </div>
                </form>
            </div>
            {Object.keys(productivoResults).length === 0 ? (
                <p className="flex justify-center font-bold text-xl">No hay resultados</p>
            ) : (
                <>
                    <div className="grid grid-cols-4 gap-4 ml-16">
                        {Object.values(productivoResults).map((productivos, key) => {
                            if (!productivos || productivos.length === 0) return null;
                            return (
                                <div class="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mb-10 " >
                                    <div class="flex justify-end px-4 pt-4 ">
                                    </div>
                                    <div class="flex flex-col items-center pb-10 ">{console.log(productivos.profile)}
                                        <img class="w-25 h-20 mb-3 rounded-full shadow-lg" src={productivos.profile} alt="Imagen de perfil" />
                                        <h5 class="mb-1 text-xl font-medium text-gray-900 dark:text-white">{productivos.colaborador}</h5>
                                        <span class="text-sm text-gray-500 dark:text-gray-400">{productivos.rol}</span>
                                        <div class="flex mt-4 md:mt-6">
                                            <Button className=' bg-ayi hover:bg-ayi text-white' onClick={() => { setOpenModal(true), setProductivo(productivos) }}>Ver mas</Button>
                                            {auth.isAdmin ? <button onClick={() => verMapa(productivos[0].mail)} class="py-2 px-4 ms-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Ver Mapa</button> : null}
                                        </div>
                                        <Modal show={openModal} onClose={() => setOpenModal(false)}>
                                            <Modal.Header><h3 className="font-bold text-lg">{`Datos de ${productivo.staff}`}</h3></Modal.Header>
                                            <Modal.Body>
                                                <div className="space-y-6">
                                                    <ul class="max-w-md space-y-1 list-none list-inside dark:text-gray-400">
                                                        <li class=" font-bold text-lg">
                                                            {`Legajo: ${productivo.legajo}`}
                                                        </li>
                                                        <li class=" font-bold text-lg">
                                                            {`Rol: ${productivo.rol}`}
                                                        </li>
                                                        <li class=" font-bold text-lg">
                                                            {`Manager: ${productivo.manager}`}
                                                        </li>
                                                        <li class=" font-bold text-lg">
                                                            {`Lider: ${productivo.lider}`}
                                                        </li>
                                                        <li class=" font-bold text-lg">
                                                            {`Lugar: ${productivo.lugar}`}
                                                        </li>
                                                        <li class=" font-bold text-lg">
                                                            {`Ingreso: ${productivo.fechaDeIngreso}`}
                                                        </li>
                                                        <li class=" font-bold text-lg">
                                                            {`UES: ${productivo.ues}`}
                                                        </li>
                                                        <li class=" font-bold text-lg">
                                                            {`Mail: ${productivo.mail}`}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <Button color="gray" onClick={() => setOpenModal(false)}>
                                                    Cerrar
                                                </Button>
                                            </Modal.Footer>
                                        </Modal>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                </>
            )
            }
        </>
    );
}

export default BuscarTalento2
