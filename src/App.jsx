import React from 'react';
import ReactDOM from 'react-dom';
import { useState } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import AuthLayout from './layouts/AuthLayout'
import Login from './pages/Login'
import { AuthProvider } from './context/AuthProvider'
import { ColaboradoresProvider } from './context/ColaboradoresProvider'
import { ProductivosUENProvider } from './context/ProductivosUENProvider'
import { CoProductivosUESProvider } from './context/CoProductivosUESProvider'
import './App.css'
import RutaProtegida from './layouts/RutaProtegida'
import Colaboradores from './pages/Colaboradores'
import { TalentoProvider } from './context/TalentoProvider'
import Mapa from './pages/Mapa'
import BuscarTalento from './pages/BuscarTalento'
import CoProductivosUES from './pages/CoProductivosUES'
import RutaProtegidaUES from './layouts/RutaProtegidaUES'
import Registro from './pages/Registro'

function App() {

  return (
    <BrowserRouter>
      <AuthProvider>
        <ProductivosUENProvider>
          <CoProductivosUESProvider>
          <TalentoProvider>
            <Routes>
              <Route path='/' element={<AuthLayout />}>
                <Route index element={<Login />} />
              </Route>
              <Route path='/colaboradores' element={<RutaProtegida />}>
                <Route index element={<Colaboradores />} />
                <Route path='mapa' element={<Mapa />} />
                <Route path='busqueda' element={<BuscarTalento />} />
                <Route path='registro' element={<Registro/>}/>
              </Route>
              <Route path='/coProductivos' element={<RutaProtegidaUES />}>
                <Route index element={<CoProductivosUES />} />
                <Route path='registro' element={<Registro/>}/>
              </Route>
            </Routes>
          </TalentoProvider>
          </CoProductivosUESProvider>
        </ProductivosUENProvider>
      </AuthProvider>
    </BrowserRouter>
  )
}

export default App
