import React from 'react';

import {useState,useEffect,createContext} from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"



const CoProductivosUESContext = createContext()

const CoProductivosUESProvider = ({children}) => {

    const [coProductivosUEN,setCoProductivosUEN] = useState({})
    const [cargando,setCargando] = useState(true)

    const navigate = useNavigate()

    useEffect(()=>{
        const obtenerCoProductivos = async ()=>{
            try {
                const token = localStorage.getItem("token")

            const cofing = {
                headers:{
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }
            //const {data} = await axios.get('https://apimapatalentos.onrender.com/api/productivosUEN/UEN/',cofing)
            const {data} = await axios.get('https://apimapatalentos.onrender.com/api/coProductivosUES/UES/',cofing)
            setCoProductivosUEN(data)
            console.log(data)
            } catch (error) {
                console.log(error)
            }

            setCargando(false)
        }
        obtenerCoProductivos()
        
    },[])
  return (
    <CoProductivosUESContext.Provider value={{
        coProductivosUEN,
        cargando
    }}>
        {children}
    </CoProductivosUESContext.Provider>
  )
}

export{CoProductivosUESProvider} 

export default CoProductivosUESContext
