import {useState,useEffect,createContext} from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"



const ColaboradoresContext = createContext()

const ColaboradoresProvider = ({children}) => {

    const [colaboradores,setColaboradores] = useState({})
    const [cargando,setCargando] = useState(true)

    const navigate = useNavigate()

    useEffect(()=>{
        const obtenerColaboradores = async ()=>{
            try {
                const token = localStorage.getItem("token")

            const cofing = {
                headers:{
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }
            const {data} = await axios.get('https://apimapatalentos.onrender.com/api/colaboradores/',cofing)
            setColaboradores(data)
            } catch (error) {
                console.log(error)
            }

            setCargando(false)
        }
        obtenerColaboradores()
        
    },[])
  return (
    <ColaboradoresContext.Provider value={{
        colaboradores,
        cargando
    }}>
        {children}
    </ColaboradoresContext.Provider>
  )
}

export{ColaboradoresProvider} 

export default ColaboradoresContext
