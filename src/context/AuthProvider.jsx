import React from 'react';

import { useState, useEffect, createContext } from "react"
import axios from "axios"

const AuthContext = createContext()

const AuthProvider = ({ children }) => {

    const [auth, setAuth] = useState({})
    const [loading, setLoading] = useState(true)

    
        const autenticarUsuario = async () => {
            try {
                const token = localStorage.getItem("token")

                const cofing = {
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`
                    }
                }

                //await axios.get('https://apimapatalentos.onrender.com/api/usuarios/perfil',cofing)
                const { data } = await axios.get('https://apimapatalentos.onrender.com/api/usuarios/perfil', cofing)

                //setAuth(data)
                setAuth(data)
                console.log(data)
                
                
                //setCargando(false)

            } catch (error) {
                console.log(error)
            }  finally{
                setLoading(false)
            }
        
        
        }
            
            useEffect(() => {
                autenticarUsuario()
    }, [])

return (
    <AuthContext.Provider value={{
        auth,
        setAuth,
        loading

    }}>
        {children}
    </AuthContext.Provider>
)

}

export { AuthProvider }

export default AuthContext