import React from 'react';
import {useState,useEffect,createContext} from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"



const TalentoContext = createContext()

const TalentoProvider = ({children}) => {

    const [talento,setTalento] = useState({})
    const [cargando,setCargando] = useState(true)

    const navigate = useNavigate()

    useEffect(()=>{
        const obtenerTalento = async ()=>{
            try {
                const token = localStorage.getItem("token")

            const cofing = {
                headers:{
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }

            //const {data} = await axios.get('https://apimapatalentos.onrender.com/api/talentos/',cofing)
            const {data} = await axios.get('https://apimapatalentos.onrender.com/api/talentos/',cofing)
            setTalento(data)
            } catch (error) {
                console.log(error)
            }

            setCargando(false)
        }
        obtenerTalento()
        
    },[])
  return (
    <TalentoContext.Provider value={{
        talento,
        cargando
    }}>
        {children}
    </TalentoContext.Provider>
  )
}

export{TalentoProvider} 

export default TalentoContext
