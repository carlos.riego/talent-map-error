import React from 'react';

import {useState,useEffect,createContext} from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"



const ProductivosUENContext = createContext()

const ProductivosUENProvider = ({children}) => {

    const [productivosUEN,setProductivosUEN] = useState({})
    const [cargando,setCargando] = useState(true)

    const navigate = useNavigate()

    useEffect(()=>{
        const obtenerProductivos = async ()=>{
            try {
                const token = localStorage.getItem("token")

            const cofing = {
                headers:{
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }
            //const {data} = await axios.get('https://apimapatalentos.onrender.com/api/productivosUEN/UEN/',cofing)
            const {data} = await axios.get('https://apimapatalentos.onrender.com/api/productivosUEN/UEN/',cofing)
            setProductivosUEN(data)
            console.log(data)
            } catch (error) {
                console.log(error)
            }

            setCargando(false)
        }
        obtenerProductivos()
        
    },[])
  return (
    <ProductivosUENContext.Provider value={{
        productivosUEN,
        cargando
    }}>
        {children}
    </ProductivosUENContext.Provider>
  )
}

export{ProductivosUENProvider} 

export default ProductivosUENContext
