import { useContext } from "react";
import TalentoContext from "../context/TalentoProvider";


const useTalento = () =>{
    return useContext(TalentoContext)
}

export default useTalento