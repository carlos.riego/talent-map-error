import { useContext } from "react";
import ProductivosUENContext from "../context/ProductivosUENProvider";

const useProductivosUEN = () =>{
    
    return useContext(ProductivosUENContext)
}

export default useProductivosUEN