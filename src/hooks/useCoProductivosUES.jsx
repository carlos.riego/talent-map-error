import { useContext } from "react";
import CoProductivosUESContext from "../context/CoProductivosUESProvider";

const useCoProductivosUES = () =>{
    
    return useContext(CoProductivosUESContext)
}

export default useCoProductivosUES