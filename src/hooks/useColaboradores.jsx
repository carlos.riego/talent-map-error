import { useContext } from "react";
import ColaboradoresContext from "../context/ColaboradoresProvider";


const useColaboradores = () =>{
    
    return useContext(ColaboradoresContext)
}

export default useColaboradores