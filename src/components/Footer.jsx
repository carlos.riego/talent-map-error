import React from 'react'
import { Footer } from "flowbite-react";


const Foter = () => {

    return (
        <Footer container className='bg-ayi mt-5 rounded-none sticky top-[100vh]'>
            <div className="w-full text-center">
                <div className="w-full justify-between sm:flex sm:items-center sm:justify-between">
                    <Footer.Brand
                        href="https://flowbite.com"
                        src="https://ayi.group/wp-content/uploads/2020/12/ayi-logo-blanco.png"
                        alt="Flowbite Logo"
                    />
                    <Footer.LinkGroup>
                    </Footer.LinkGroup>
                </div>
                <Footer.Divider />
                <Footer.Copyright href="#" className=' text-white text-base font-bold' by="Flowbite™" year={2022} />
            </div>
        </Footer>

    )


}

export default Foter