import React from 'react'
import { useState, useEffect } from 'react'
import useAuth from "../hooks/useAuth"
import { useNavigate, useLocation } from 'react-router-dom'
//import useColaboradores from '../hooks/useColaboradores'
import useProductivosUEN from '../hooks/useProductivosUEN'
import axios from "axios"
import { Divider } from '@nextui-org/react'




const Header = () => {
    const { auth } = useAuth()
    const navigate = useNavigate()
    const { colaboradores, loading } = useProductivosUEN()
    const [openModal, setOpenModal] = useState(false);
    const [foto, setFoto] = useState('')

   const Auth = JSON.parse(localStorage.getItem("Auth"))



    const Funcion = () => {
        localStorage.clear()
        location.reload()
        navigate("/")
    }



    const cargaMasiva = () => {


        try {

            const token = localStorage.getItem("token")
            const mail = localStorage.getItem("mail")



            const cofing = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }

            colaboradores.forEach(element => {
                const contraseña = element.colaborador?.replace(/ /g, "").toLowerCase() + "2024"
                const data = {
                    email: element.mail,
                    password: contraseña
                }
                console.log(data)
                axios.post(`https://apimapatalentos.onrender.com/api/usuarios/registro`, data, cofing)
                //axios.put(`http://localhost:4000/api/usuarios/registro`, data, cofing)
            });
        } catch (error) {
            console.log(error)
        }

    }

    const miMapa = () => {
        localStorage.setItem("mail", Auth.email)
        navigate("/colaboradores/mapa")
    }

    const miBusqueda = () => {
        navigate("/colaboradores/busqueda")
    }

    const productivosUEN = () => {
        navigate("/colaboradores")
    }
    const coProductivosUES = () => {
        navigate("/coProductivos")
    }

    const actualizarPerfil = () => {
        try {

            const token = localStorage.getItem("token")
            const mail = localStorage.getItem("mail")


            const cofing = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }

            const data = {
              perfil: foto
            }
            axios.post(`https://apimapatalentos.onrender.com/api/usuarios/cambiarProfile/${Auth._id}`, data, cofing)
            axios.post(`https://apimapatalentos.onrender.com/api/productivosUEN/cambiarProfile/${Auth._id}`, data, cofing)
            axios.post(`https://apimapatalentos.onrender.com/api/coProductivosUES/cambiarProfile/${Auth._id}`, data, cofing)


        } catch (error) {
            console.log(error)
        }
    }


    if (loading) {
        return <div>Cargando....</div>
    }



    return (
        <>
            <div className="navbar bg-ayi shadow-md">
                <div className="flex-1">
                    <a>
                        <img
                            src="https://ayi.group/wp-content/uploads/2020/12/ayi-logo-blanco.png"
                            className="mr-3 h-6 sm:h-9"
                        />
                    </a>
                </div>
                <div className="navbar-center hidden lg:flex justify-center text-center">
                    <ul className="menu menu-horizontal px-1">
                        <li><button className=' text-white text-md font-bold hover:underline' onClick={() => miBusqueda()}>Buscar Talentos</button></li>
                        <li><button className=' text-white text-md font-bold hover:underline' onClick={() => productivosUEN()}>Productivos UEN</button></li>
                        <li><button className=' text-white text-md font-bold hover:underline' onClick={() => miMapa()}>Mi Mapa de Talentos</button></li>
                        <li><button className=' text-white text-md font-bold hover:underline' onClick={() => { coProductivosUES() }}>Co-Productivos UES</button></li>
                    </ul>
                </div>
                <dialog id="my_modal_1" className="modal">
                    <div className="modal-box">
                        <div className="space-y-6">
                            <h2 className="card-title">Cambiar foto de perfil</h2>
                            <p>Pega la URL <input type="text" onChange={(e)=>{setFoto(e.target.value)}}/></p>
                        </div>
                        <div className="modal-action">
                            <form method="dialog">
                                <button className=" bg-green-500 hover:bg-green-700 text-white rounded-md w-24 h-14 mr-3 font-bold" onClick={actualizarPerfil}>Actualizar</button>
                                <button className="bg-ayi hover:bg-ayi text-white rounded-md w-24 h-14 mr-3 font-bold">Cerrar</button>
                            </form>
                        </div>
                    </div>
                </dialog>
                <div className="flex-none gap-2">
                    <div className="dropdown dropdown-end">
                        <div tabIndex={0} role="button" className="btn btn-ghost btn-circle avatar">
                            <div className="w-10 rounded-full">
                                <img alt="Imagen de perfil" src={Auth.perfil} />
                            </div>
                        </div>
                        
                        <ul tabIndex={0} className="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
                            <div className="divider">USUARIO</div>
                            <li>
                                <p className="justify-between pointer-events-none">
                                    {Auth.email}
                                </p>
                            </li>
                            <div className="divider">ADMIN</div>
                            <li>{useLocation().pathname === "/coProductivos" ? 
                            <button onClick={() => { navigate("/colaboradores/registro") }}>Agregar nuevo usuario</button> :
                            <button onClick={() => { navigate("registro") }}>Agregar nuevo usuario</button>
                            }
                                
                            </li>
                            <li><button onClick={() => { cargaMasiva() }}>Registrar todos los usuarios</button></li>
                            <div className="divider">OPCIONES</div>
                            <li>
                                <button onClick={() => { document.getElementById('my_modal_1').showModal() }}>Cambiar foto de perfil</button>
                            </li>
                            <li><button onClick={() => Funcion()}>Cerrar Sesion</button></li>
                        </ul>
                    </div>
                </div>
            </div>

        </>
    )
}

export default Header
