import axios from "axios"
import { useEffect, useRef, useState } from "react"
import { Spinner, Card, Dropdown } from 'flowbite-react';
import { Modal, Button, FloatingLabel } from 'flowbite-react'
import { Viewer, Worker, PdfJsBackend } from '@react-pdf-viewer/core';
//import { PdfJsBackend } from '@react-pdf-viewer/pdfjs';
import 'react-pdf/dist/Page/AnnotationLayer.css'
const workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/3.11.174/pdf.worker.min.js';

const Modals = (competencia) => {


    const [nombre, setNombre] = useState('')
    const [nivel, setNivel] = useState('')
    const [experiencia, setExperiencia] = useState('')
    const [certificado, setCertificado] = useState(null)
    const [categoria, setCategoria] = useState(null)
    const [openModal, setOpenModal] = useState(false)
    const [openModalPdf, setOpenModalPdf] = useState(false)
    const [openModalInput, setOpenModalInput] = useState(false)
    const [deleteResponse, setDeleteResponse] = useState({})
    const [modResponse, setModResponse] = useState(false)
    const [categorias, setCategorias] = useState({})
    const [data, setData] = useState({})

    const Niveles = ["Inicial", "Intermedio", "Avanzado", "Certificado"]
    const Experiencias = ["En Formacion", "0-6 Meses", "1 año", "2 años", "3 años", "Más de 5 años"]

 

    const obtenerTalento = async () => {
        try {
            const token = localStorage.getItem('token');
            const mail = localStorage.getItem('mail');
            const config = {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            };

            const response = await axios.get(`https://apimapatalentos.onrender.com/api/talentos/ver/${mail}`, config);
            setData(response.data);
        } catch (error) {
            console.log('Error al obtener talentos:', error);
        }
    };
    const handleSubmits = async e => {
        e.preventDefault()

        try {

            const token = localStorage.getItem("token")

            const cofing = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }
 
            const { mod } = await axios.post(`https://apimapatalentos.onrender.com/api/talentos/modificar/${data[0]._id}`, {
                competencias: [
                    {
                        nombre,
                        certificado,
                        experiencia,
                        nivel,
                        categoria

                    }]
            }, cofing)
            setOpenModal(false)
            setModResponse(true)
        } catch (error) {
            console.log(error)
        }
    }

    const handleDelete = async (id, id_comp) => {
        const token = localStorage.getItem("token")
        const cofing = {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        }

        try {
            const { data } = await axios.post(`https://apimapatalentos.onrender.com/api/talentos/eliminar/${id}/${id_comp}`, {}, cofing);
            setDeleteResponse(data)
        } catch (error) {
            console.log(error);
        }
    }

    const fetchCategoria = async () => {
        const token = localStorage.getItem("token")
        const cofing = {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        }

        try {
            const { data } = await axios.get(`https://apimapatalentos.onrender.com/api/categorias/ver`, cofing);
            setCategorias(data)
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        obtenerTalento()
    }, [deleteResponse])

    useEffect(() => {
        obtenerTalento()
        setModResponse(false)
    }, [modResponse])

    useEffect(() => {
        obtenerTalento()
        fetchCategoria()
        console.log(data)


    }, [])
    useEffect(() => {

        console.log(certificado)


    }, [certificado])

    return (
        <>


            <div className="w-screen">
                <div className=" font-bold text-3xl divider underline uppercase">{competencia.competencia}</div>
                <button className="btn btn-accent mt-5 mr-5 float-right" onClick={() => { setOpenModal(true) }}>Agregar</button>
                <div className="grid grid-cols-3 gap-4">
                    {data && data.length > 0 && competencia && Object.values(data[0].competencias).map((elemento, index) => {
                        return data[0].competencias[index].categoria == competencia.competencia ?
                            <>
                                <div className="card w-96 bg-base-100 shadow-xl ml-5 overflow-hidden">
                                    <div className="card-body">
                                        <h2 className="card-title">{elemento.nombre}</h2>
                                        <p>{`Nivel: ${elemento.nivel}`}</p>
                                        <p>{`Experiencia: ${elemento.experiencia}`}</p>


                                        <div className="card-actions justify-end">
                                            <button className="btn btn-error" onClick={() => { handleDelete(data[0]._id, data[0]?.competencias[index]?._id) }}>Eliminar</button>
                                            <button button className="btn btn-green" onClick={() => { setOpenModalInput(true) }}>Agregar Certificado</button>
                                            {certificado ?
                                                <button className="btn btn-default" onClick={() => { setOpenModalPdf(true) }}>Ver Certificado</button> :
                                                null}
                                        </div>
                                        <Modal show={openModalPdf} onClose={() => { setOpenModalPdf(false) }} size={"5xl"} >
                                            <Modal.Header>Header</Modal.Header>
                                            <Modal.Body>
                                                <div className="space-y-6">

                                                    <div>
                                                        {certificado &&
                                                            <Worker workerUrl={workerSrc}>
                                                                <Viewer fileUrl={URL.createObjectURL(certificado)} />
                                                            </Worker>}
                                                    </div>

                                                </div>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <Button color="gray" onClick={() => { setOpenModalPdf(false) }}>
                                                    Cerrar
                                                </Button>
                                            </Modal.Footer>
                                        </Modal>
                                    </div>

                                </div>
                            </>
                            : null
                    })}
                    <Modal show={openModal} onClose={() => { setOpenModal(false) }} size={"5xl"} >
                        <Modal.Header>Agregar nuevo</Modal.Header>
                        <Modal.Body>
                            <div className="space-y-6">
                                <form action="" onSubmit={handleSubmits}>
                                    <div>
                                        <div className=" my-5 flex">
                                            <Dropdown
                                                label="Nombre del talento"
                                                name="idioma"
                                                className="uppercase text-gray-700 block text-lg font-bold"
                                            >
                                                {categorias && categorias.length > 0 && categorias.map(item => item[competencia.competencia]).flat().map((dato, index) => (
                                                    <Dropdown.Item key={index} onClick={() => { setNombre(dato), setCategoria(competencia.competencia) }}>
                                                        {dato}
                                                    </Dropdown.Item>
                                                ))}
                                            </Dropdown>
                                            <label type="text" for="idioma" className="bg-gray-100 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed">{nombre}</label>
                                        </div>
                                        <div className="divider"></div>
                                        <div className=" my-5 flex">
                                            <Dropdown
                                                label="Nivel de experiencia"
                                                name="experiencia"
                                                className="uppercase text-gray-700 block text-lg font-bold mr-5"
                                            >
                                                {Niveles.map((dato, index) => (
                                                    <Dropdown.Item key={index} onClick={() => { setNivel(dato) }}>
                                                        {dato}
                                                    </Dropdown.Item>
                                                ))}
                                            </Dropdown>
                                            <label type="text" for="experiencia" className="bg-gray-100 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed">{nivel}</label>
                                        </div>
                                        <div className="divider"></div>
                                        <div className=" my-5 flex">
                                            <Dropdown
                                                label="Tiempo de competencia"
                                                name="tiempo"
                                                className="uppercase text-gray-700 block text-lg font-bold mr-5"
                                            >
                                                {Experiencias.map((dato, index) => (
                                                    <Dropdown.Item key={index} onClick={() => { setExperiencia(dato) }}>
                                                        {dato}
                                                    </Dropdown.Item>
                                                ))}
                                            </Dropdown>
                                            <label type="text" for="tiempo" className="bg-gray-100 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 cursor-not-allowed">{experiencia}</label>
                                        </div>
                                        <div className=" my-5 flex">
                                            <input type="file" />
                                        </div>
                                    </div>
                                    <div className="divider"></div>
                                    <Button color="green" type="submit">
                                        Actualizar
                                    </Button>
                                </form>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button color="gray" onClick={() => { setOpenModal(false) }}>
                                Cerrar
                            </Button>
                        </Modal.Footer>
                    </Modal>



                </div>
            </div>





        </>)
}

export default Modals